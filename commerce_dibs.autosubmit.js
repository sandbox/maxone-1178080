/**
 * @file
 * Handles autosubmitting the DIBS payment form.
 */
(function ($) {
  Drupal.behaviors.commerceDibsAutoSubmit = {
    attach: function(context, settings) {
      $("#commerce-dibs-redirect-form").submit();
    }
  };
})(jQuery);
