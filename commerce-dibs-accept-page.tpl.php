<?php

/**
 * @file dibs-accept-page.tpl.php
 *
 * Theme implementation to display the dibs accept page
 *
 * @see template_preprocess()
 * @see template_preprocess_dibs_accept_page()
 */
?>
<?php print t('Your payment was accepted. Your order number is '); ?>